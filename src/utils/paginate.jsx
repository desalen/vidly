import _ from 'lodash'; //underscroe

export function paginate(items, pageNumber, pageSize) {
    const startIndex = (pageNumber - 1) * pageSize;
    //instad of :
    // _.slice(items, startIndex); 
    // .take()
    // use:
    return _(items)
        .slice(startIndex)
        .take(pageSize)
        .value();
}