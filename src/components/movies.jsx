import React, { Component } from 'react';
// import functions like:
import { getMovies } from '../services/fakeMovieService';
import {paginate} from '../utils/paginate';

import Like from './common/like';
import Pagination from './common/pagination';

class Movies extends Component {
    state = {
        movies: getMovies(),
        currentPage:1,
        pageSize: 4//4 movies in each page
    }
    render() {
        const { length: count } = this.state.movies;
        const{currentPage,pageSize,movies:allMovies}=this.state; 
        //because we allready have movies prop
        //we will change the name of movies to allMovies
        
        if (count === 0)
            return <p>there is no movies now!</p>

        const movies=paginate(allMovies,currentPage,pageSize);

        return (
            <React.Fragment>
                <p>showing {count} of movies </p>
                <div className="panel panel-default moviesTable">
                    <div className="panel-heading">Panel heading</div>
                    <div className="panel-body">

                    </div>

                    <table className="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Genere</th>
                                <th>Rate</th>
                                <th>Stock</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {movies.map(movie => (
                                <tr key={movie._id}>
                                    <td>{movie.title}</td>
                                    <td>{movie.genre.name}</td>
                                    <td>{movie.dailyRentalRate}</td>
                                    <td>{movie.numberInStock}</td>
                                    <td>
                                        <Like liked={movie.liked}
                                            onLikedToggle={() => this.handleLike(movie)} />
                                    </td>
                                    <td><button
                                        onClick={() => this.handleDelete(movie)}
                                        className="btn btn-danger">Delete</button></td>
                                </tr>
                            ))}

                        </tbody>
                    </table>
                </div>
                <Pagination
                    itemsCount={count}
                    pageSize={pageSize}
                    currentPage={currentPage}
                    onPageChange={this.handlePageChange} />
            </React.Fragment>

        );
    } 
    handleDelete = movie => {
        const movies = this.state.movies.filter(m => m._id !== movie._id);
        // this.setState({movies:movies})
        this.setState({ movies })

    }
    handleLike = movie => {
        //because we dont want to change the state directly 
        //we will take a clone of the movie/state arrary
        //spread operator for clone the array  

        const movies = [...this.state.movies];
        const index = movies.indexOf(movie);
        movies[index] = { ...movie };
        movies[index].liked = !movies[index].liked;
        this.setState({ movies });
    }
    handlePageChange = page => {
        this.setState({currentPage:page})
    }
}

export default Movies;