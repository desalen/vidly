import React from 'react';

//stateless component
//Input:isLiked?
//Output:onClick

const Like = ({ onLikedToggle, liked }) => {
    let classes = "fa fa-heart";
    classes += liked ? "" : "-o";

    return (
        <div>
            <i onClick={onLikedToggle} style={{ cursor: "pointer" }} className={classes} ></i>
        </div>
    );
}
export default Like;