import React from 'react';
import _ from 'lodash'; //underscore library

//everytime we are going to build an reusable component 
//we need to think its interface
//what are the input and outputs

//Input:which page,totalItems?
//Output:movetoPage function

const Pagination = props => {  
    const { itemsCount, pageSize,currentPage,onPageChange } = props; 
    const pageCount =Math.ceil(itemsCount / pageSize); 
    if (pageCount === 1) return null;
    const pages = _.range(1, pageCount + 1);
      
    return (
        <nav aria-label="...">
            <ul className="pagination pagination-md">
                {pages.map(page =>
                    <li className={page===currentPage?"page-item active":"page-item"} key={page}>
                        <a className="page-link"  onClick={()=>onPageChange(page)} >{page}</a>
                     </li>)}
            </ul>
        </nav>
    );
}


export default Pagination;